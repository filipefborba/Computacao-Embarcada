/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */

#define YEAR        2018
#define MONTH       3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      59
#define SECOND      0

/**
* LEDs
*/
#define LED0_PIO_ID	    ID_PIOC
#define LED0_PIO        PIOC
#define LED0_PIN		8
#define LED0_PIN_MASK   (1<<LED0_PIN)

#define LED1_PIO_ID	    ID_PIOA
#define LED1_PIO        PIOA
#define LED1_PIN		0
#define LED1_PIN_MASK   (1<<LED1_PIN)

/**
* Botao
*/
#define BUT0_PIO_ID			  ID_PIOA
#define BUT0_PIO			  PIOA
#define BUT0_PIN			  11
#define BUT0_PIN_MASK		  (1 << BUT0_PIN)
#define BUT0_DEBOUNCING_VALUE 79

#define BUT1_PIO_ID			  ID_PIOD
#define BUT1_PIO			  PIOD
#define BUT1_PIN			  28
#define BUT1_PIN_MASK		  (1 << BUT1_PIN)

#define BUT2_PIO_ID			  ID_PIOC
#define BUT2_PIO			  PIOC
#define BUT2_PIN			  31
#define BUT2_PIN_MASK		  (1 << BUT2_PIN)

#define BUT3_PIO_ID			  ID_PIOA
#define BUT3_PIO			  PIOA
#define BUT3_PIN			  19
#define BUT3_PIN_MASK		  (1 << BUT3_PIN)


#include <asf.h>

#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"

/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

volatile uint8_t flag_led0 = 1;
volatile uint8_t flag_led1 = 1;
volatile uint8_t minute_flag = 0;
volatile uint8_t set_alarm_mode_flag = 0;
volatile uint8_t alarm_minutes = 0;

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

void pin_toggle(Pio *pio, uint32_t mask);
void RTC_init(void);
void RTC_Handler(void);
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
void BUT_init(void);
void LED_init(void);
void change_time(uint32_t hour, uint32_t min);


void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask)) {
		pio_clear(pio, mask);
	}
	else {
		pio_set(pio,mask);
	}
}

/**
*  Interrupt handler for TC0 interrupt.
*/
void TC0_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrupo foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 0);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	if(flag_led0 == 0)
		pin_toggle(LED0_PIO, LED0_PIN_MASK);
}


/**
* Configura TimerCounter (TC) para gerar uma interrupcao no canal (ID_TC e TC_CHANNEL)
* na taxa de especificada em freq.
*/
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	/* Configura o PMC */
	/* O TimerCounter  meio confuso
	o uC possui 3 TCs, cada TC possui 3 canais
	TC0 : ID_TC0, ID_TC1, ID_TC2
	TC1 : ID_TC3, ID_TC4, ID_TC5
	TC2 : ID_TC6, ID_TC7, ID_TC8
	*/
	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  freq Mhz e interrupco no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrupco no TC canal 0 */
	/* Interrupo no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}


/**
*  Handle Interrupcao botao
*/
void Button0_Handler(void)
{
	flag_led0 = !flag_led0;
}

void Button1_Handler(void)
{
	set_alarm_mode_flag++;
}

void Button2_Handler(void)
{
	alarm_minutes--;
}

void Button3_Handler(void)
{
	alarm_minutes++;
}


/**
* Configura o RTC para funcionar com interrupcao de alarme
*/
void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MONTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN | RTC_IER_SECEN);

}


/**
* \brief Interrupt handler for the RTC. Refresh the display.
*/
void RTC_Handler(void)
{
	uint32_t ul_status = rtc_get_status(RTC);
	
	// get current time
	uint32_t h, m, s;
	rtc_get_time(RTC,&h,&m,&s);
	
	/*
	*  Verifica por qual motivo entrou
	*  na interrupcao, se foi por segundo
	*  ou Alarm
	*/
	if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		minute_flag++;
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
	}
	
	/* Time or date alarm */
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
			rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
			
			flag_led0 = 0;
			if (flag_led0 == 0) {
				pmc_enable_periph_clk(ID_TC0);
			} else {
				pmc_disable_periph_clk(ID_TC0);
			}

		}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
	
}

void init_screen_text(void) {
	uint8_t stringLCD[256];
	sprintf(stringLCD, "%d:%d", HOUR, MINUTE);
	gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
	gfx_mono_draw_string(stringLCD, 0, 0, &sysfont);
}

void change_time(uint32_t hour, uint32_t min) {
	
	uint8_t stringLCD[256];
	gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
	if (hour > 10 && min < 10){
		sprintf(stringLCD, "%d:0%d", hour, min);	
	}
	else if (hour < 10 && min > 10){
		sprintf(stringLCD, "0%d:%d", hour, min);
	}
	else if (hour < 10 && min < 10){
		sprintf(stringLCD, "0%d:0%d", hour, min);
	}
	else {
		sprintf(stringLCD, "%d:%d", hour, min);
	}
	gfx_mono_draw_string(stringLCD, 0, 0, &sysfont);
}

void enable_alarm_mode(void) {
	alarm_minutes = 0;
	change_time(0, 0);
	pin_toggle(LED1_PIO, LED1_PIN_MASK);
}

void disable_alarm_mode(void) {
	pin_toggle(LED1_PIO, LED1_PIN_MASK);
	uint32_t h, m, s;
	rtc_get_time(RTC,&h,&m,&s);
	if (m + alarm_minutes >= 60) {
		rtc_set_time_alarm(RTC, 1, h+1, 1, m+alarm_minutes-60, 1, 0);
	} else {
		rtc_set_time_alarm(RTC, 1, h, 1, m+alarm_minutes, 1, 0);
	}
}

void enable_clock_mode(void) {
	uint32_t h, m, s;
	rtc_get_time(RTC,&h,&m,&s);
	change_time(h, m);
}

/**
* @Brief Inicializa o pino do BUT
*/
void BUT_init(void){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(BUT0_PIO_ID);
	pio_set_input(BUT0_PIO, BUT0_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	
	pmc_enable_periph_clk(BUT1_PIO_ID);
	pio_set_input(BUT1_PIO, BUT1_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	
	pmc_enable_periph_clk(BUT2_PIO_ID);
	pio_set_input(BUT2_PIO, BUT2_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	
	pmc_enable_periph_clk(BUT3_PIO_ID);
	pio_set_input(BUT3_PIO, BUT3_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrupo */
	pio_enable_interrupt(BUT0_PIO, BUT0_PIN_MASK);
	pio_handler_set(BUT0_PIO, BUT0_PIO_ID, BUT0_PIN_MASK, PIO_IT_FALL_EDGE, Button0_Handler);
	
	pio_enable_interrupt(BUT1_PIO, BUT1_PIN_MASK);
	pio_handler_set(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN_MASK, PIO_IT_FALL_EDGE, Button1_Handler);
	
	pio_enable_interrupt(BUT2_PIO, BUT2_PIN_MASK);
	pio_handler_set(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN_MASK, PIO_IT_FALL_EDGE, Button2_Handler);
	
	pio_enable_interrupt(BUT3_PIO, BUT3_PIN_MASK);
	pio_handler_set(BUT3_PIO, BUT3_PIO_ID, BUT3_PIN_MASK, PIO_IT_FALL_EDGE, Button3_Handler);

	/* habilita interrupco do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(BUT0_PIO_ID);
	NVIC_SetPriority(BUT0_PIO_ID, 1);
	
	NVIC_EnableIRQ(BUT1_PIO_ID);
	NVIC_SetPriority(BUT1_PIO_ID, 1);
	
	NVIC_EnableIRQ(BUT2_PIO_ID);
	NVIC_SetPriority(BUT2_PIO_ID, 1);
	
	NVIC_EnableIRQ(BUT3_PIO_ID);
	NVIC_SetPriority(BUT3_PIO_ID, 1);
};

/**
* @Brief Inicializa o pino do LED
*/
void LED_init(){
	pmc_enable_periph_clk(LED0_PIO_ID);
	pio_set_output(LED0_PIO, LED0_PIN_MASK, 1, 0, 0);
	
	pmc_enable_periph_clk(LED1_PIO_ID);
	pio_set_output(LED1_PIO, LED1_PIN_MASK, 1, 0, 0);
};

int main (void)
{
	board_init();
	sysclk_init();
	delay_init();
	gfx_mono_ssd1306_init();

	/* Configura Leds */
	LED_init();

	/* Configura os botes */
	BUT_init();

	/** Configura RTC */
	RTC_init();
	
	TC_init(TC0, ID_TC0, 0, 4);
	pmc_disable_periph_clk(ID_TC0);
	
	flag_led0 = 1;
	flag_led1 = 1;
	minute_flag = 0;
	set_alarm_mode_flag = 0;
	alarm_minutes = 0;
	
	init_screen_text();
	
	while(1) {
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		if (set_alarm_mode_flag == 1) {
			enable_alarm_mode();
			set_alarm_mode_flag++;
		}
		if (set_alarm_mode_flag == 2) {
			change_time(0, alarm_minutes);
		}
		if (set_alarm_mode_flag == 3) {
			set_alarm_mode_flag = 0;
			change_time(0, alarm_minutes);
			disable_alarm_mode();
			enable_clock_mode();
		}
		
		if (minute_flag >= 60) {
			minute_flag = 0;
			uint32_t h, m, s;
			rtc_get_time(RTC,&h,&m,&s);
			change_time(h, m);
		}
		
	}
}
