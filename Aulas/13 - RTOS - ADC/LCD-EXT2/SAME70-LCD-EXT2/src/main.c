#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ioport.h"
#include "logo.h"

#define LED_PIO PIOC
#define LED_PIO_ID ID_PIOC
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)


#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

#define TOUCH_PIO PIOC
#define TOUCH_PIO_ID ID_PIOC
#define TOUCH_PIO_PIN 31
#define TOUCH_PIO_PIN_MASK (1<< TOUCH_PIO_PIN)

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL

/** Reference voltage for AFEC,in mv. */
#define VOLT_REF        (3300)

/** The maximal digital value */
/** 2^12 - 1                  */
#define MAX_DIGITAL     (4095UL)

/* Canal do sensor de temperatura */
#define AFEC_CHANNEL_TEMP_SENSOR 11

struct ili9488_opt_t g_ili9488_display_opt;


// Flag
/** The conversion data is done flag */
volatile bool is_conversion_done = false;

/** The conversion data value */
volatile uint32_t g_ul_value = 0;

volatile Bool but_flag = false;
volatile Bool touch_flag = false;

// Frequ�ncia inicial do LED piscar
volatile uint32_t freq = 1;

void but_callBack(void){
	but_flag = true;
}

void touch_callBack(void){
	touch_flag = true;
}

void pisca_led(void) {
	// Set LED
	pio_set(LED_PIO, LED_PIO_PIN_MASK);
	
	//delay_ms(200);
	
	// Clear LED
	pio_clear(LED_PIO, LED_PIO_PIN_MASK);
	
	//delay_ms(200);
}

void write_freq(uint32_t freq){
	uint8_t stringLCD[256];
	
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 215, ILI9488_LCD_WIDTH-1, 250);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	
	sprintf(stringLCD, "Frequencia: %dHz", freq);
	ili9488_draw_string(10, 230, stringLCD);
}


/**
}
/**
 * \brief Configure UART console.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}

void configure_board(void) {
	sysclk_init();
	board_init();
	ioport_init();
}

void configure_peripherals(void) {
	// Inicializa o clock dos perif�ricos
	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	pmc_enable_periph_clk(TOUCH_PIO_ID);
	
	// Configura os perif�ricos
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	pio_configure(TOUCH_PIO, PIO_INPUT, TOUCH_PIO_PIN_MASK, PIO_DEFAULT);
}

void configure_interruptions(void) {
	// Configura o interruptor
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_handler_set(BUT_PIO, ID_PIOA, BUT_PIO_PIN_MASK, PIO_IT_FALL_EDGE, &but_callBack);
	pio_enable_interrupt(TOUCH_PIO, TOUCH_PIO_PIN_MASK);
	pio_handler_set(TOUCH_PIO, ID_PIOC, TOUCH_PIO_PIN_MASK, PIO_IT_FALL_EDGE, &touch_callBack);
	
	// Configura o NVIC do PIOA
	NVIC_EnableIRQ(ID_PIOA);
	NVIC_SetPriority(ID_PIOA, 0);
	
	// Configura o NVIC DO PIOC
	NVIC_EnableIRQ(ID_PIOC);
	NVIC_SetPriority(ID_PIOC, 0);
}

void write_init_lcd(void) {
	// array para escrita no LCD
	uint8_t stringLCD[256];
	
	/* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	
	sprintf(stringLCD, "Computacao Embarcada %d", 2018);
	ili9488_draw_string(10, 300, stringLCD);
	
	sprintf(stringLCD, "Filipe Borba");
	ili9488_draw_string(10, 330, stringLCD);
}

void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask)) {
		pio_clear(pio, mask);	
	}
	else {
		pio_set(pio,mask);	
	}
}

void TC1_Handler(void){
	volatile uint32_t ul_dummy;
	
	// Indicar que a interrup��o foi satisfeita
	ul_dummy = tc_get_status(TC0, 1);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	// Muda o estado do LED
	pin_toggle(LED_PIO, LED_PIO_PIN_MASK);
}

void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	uint32_t channel = 1;

	/* Configura o TC */
	/*
	TC0 : ID_TC0, ID_TC1, ID_TC2
	TC1 : ID_TC3, ID_TC4, ID_TC5
	TC2 : ID_TC6, ID_TC7, ID_TC8
	*/
	pmc_enable_periph_clk(ID_TC);

	// Inicializa o TC para operar na frequ�ncia que queremos
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrupcao no TC canal 0 */
	/* Interrupcao no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal do TC */
	tc_start(TC, TC_CHANNEL);
}

/**
 * \brief AFEC interrupt callback function.
 */
static void AFEC_Temp_callback(void)
{
	g_ul_value = afec_channel_get_value(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);
	is_conversion_done = true;
}

/** 
 * converte valor lido do ADC para temperatura em graus celsius
 * input : ADC reg value
 * output: Temperature in celsius
 */
static int32_t convert_adc_to_temp(int32_t ADC_value){
  
  int32_t ul_vol;
  int32_t ul_temp;
  
	ul_vol = ADC_value * VOLT_REF / MAX_DIGITAL;

  /*
   * According to datasheet, The output voltage VT = 0.72V at 27C
   * and the temperature slope dVT/dT = 2.33 mV/C
   */
  ul_temp = (ul_vol - 720)  * 100 / 233 + 27;
  return(ul_temp);
}

static void config_ADC_TEMP(void){
/************************************* 
   * Ativa e configura AFEC
   *************************************/  
  /* Ativa AFEC - 0 */
	afec_enable(AFEC0);

	/* struct de configuracao do AFEC */
	struct afec_config afec_cfg;

	/* Carrega parametros padrao */
	afec_get_config_defaults(&afec_cfg);

	/* Configura AFEC */
	afec_init(AFEC0, &afec_cfg);
  
	/* Configura trigger por software */
	afec_set_trigger(AFEC0, AFEC_TRIG_SW);
  
	/* configura call back */
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_11,	AFEC_Temp_callback, 1); 
   
	/*** Configuracao espec�fica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg;
	afec_ch_get_config_defaults(&afec_ch_cfg);
	afec_ch_cfg.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, AFEC_CHANNEL_TEMP_SENSOR, &afec_ch_cfg);
  
	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	 down to 0.
	 */
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_TEMP_SENSOR, 0x200);

	/***  Configura sensor de temperatura ***/
	struct afec_temp_sensor_config afec_temp_sensor_cfg;

	afec_temp_sensor_get_config_defaults(&afec_temp_sensor_cfg);
	afec_temp_sensor_set_config(AFEC0, &afec_temp_sensor_cfg);

	/* Selecina canal e inicializa convers�o */  
	afec_channel_enable(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);
}


int main(void)
{	
	/* Initialize the board. */
	configure_board();
	
	// Inicializa os perif�ricos
	configure_peripherals();
	
	// Inicializa as interrup��es do sistema
	configure_interruptions();
	
	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);

    /* Inicializa e configura o LCD */
	configure_lcd();

    // Imagem inicial do LCD
	write_init_lcd();
	
	// Escrever a frequ�ncia do LED
	write_freq(freq);
	
	// Inicializa o TC com a frequ�ncia inicial de 2 Hz
	TC_init(TC0, ID_TC1, 1, freq);

	/* inicializa delay */
	delay_init(sysclk_get_cpu_hz());
	
	/* inicializa e configura adc */
	config_ADC_TEMP();
	
	/* incializa convers�o ADC */
	afec_start_software_conversion(AFEC0);
	
	while (1) {
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		if(is_conversion_done == true) {
			is_conversion_done = false;
      
			printf("Temp : %d \r\n", convert_adc_to_temp(g_ul_value));
			afec_start_software_conversion(AFEC0);
			delay_s(1);
		}
	}
	return 0;
}
