/**
 * 5 semestre - Eng. da Computacao - Insper
 * Filipe BOrba
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"

#define LED_PIO PIOC
#define LED_PIO_ID ID_PIOC
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define BUT_PIO PIOA
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)
// Função para piscar o LED 5 vezes
void pisca_led(void) {
	for (int i = 0; i < 5; i++) {
		// Acende o LED
		pio_set(LED_PIO, LED_PIO_PIN_MASK);
		
		// Espera 200 ms
		delay_ms(200);
		
		// Apaga o LED
		pio_clear(LED_PIO, LED_PIO_PIN_MASK);
		
		// Espera 200 ms
		delay_ms(200);
	}
}

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	// Inicializa o clock da placa
	sysclk_init();
	
	// Desativa WatchDog
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	// Inicializa o clock dos periféricos
	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	
	// Configura os periféricos
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);

	// Super Loop
	while (1) {
		// Se o botão é apertado, chama pisca_led()
		if(!pio_get(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK)) {
			pisca_led();
		}
	}
	return 0;
}