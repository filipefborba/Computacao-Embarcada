#include <asf.h>
#include "conf_board.h"

#define configASSERT_DEFINED 1

#define TASK_MONITOR_STACK_SIZE            (2048/sizeof(portSTACK_TYPE))
#define TASK_MONITOR_STACK_PRIORITY        (tskIDLE_PRIORITY)
#define TASK_LED_STACK_SIZE                (1024/sizeof(portSTACK_TYPE))
#define TASK_LED_STACK_PRIORITY            (configMAX_PRIORITIES - 1)

extern void vApplicationStackOverflowHook(xTaskHandle *pxTask, signed char *pcTaskName);
extern void vApplicationIdleHook(void);
extern void vApplicationTickHook(void);
extern void vApplicationMallocFailedHook(void);
extern void xPortSysTickHandler(void);

/**
* LEDs
*/
#define LED0_PIO_ID	    ID_PIOC
#define LED0_PIO        PIOC
#define LED0_PIN		8
#define LED0_PIN_MASK   (1<<LED0_PIN)

#define LED1_PIO_ID	    ID_PIOA
#define LED1_PIO        PIOA
#define LED1_PIN		0
#define LED1_PIN_MASK   (1<<LED1_PIN)

#define LED2_PIO_ID	    ID_PIOC
#define LED2_PIO        PIOC
#define LED2_PIN		30
#define LED2_PIN_MASK   (1<<LED2_PIN)

#define LED3_PIO_ID	    ID_PIOB
#define LED3_PIO        PIOB
#define LED3_PIN		2
#define LED3_PIN_MASK   (1<<LED3_PIN)

/**
* Bot�o
*/
#define BUT0_PIO_ID			  ID_PIOA
#define BUT0_PIO			  PIOA
#define BUT0_PIN			  11
#define BUT0_PIN_MASK		  (1 << BUT0_PIN)
#define BUT0_DEBOUNCING_VALUE 79

#define BUT1_PIO_ID			  ID_PIOD
#define BUT1_PIO			  PIOD
#define BUT1_PIN			  28
#define BUT1_PIN_MASK		  (1 << BUT1_PIN)

#define BUT2_PIO_ID			  ID_PIOC
#define BUT2_PIO			  PIOC
#define BUT2_PIN			  31
#define BUT2_PIN_MASK		  (1 << BUT2_PIN)

#define BUT3_PIO_ID			  ID_PIOA
#define BUT3_PIO			  PIOA
#define BUT3_PIN			  19
#define BUT3_PIN_MASK		  (1 << BUT3_PIN)


void but_callback(void);
void init_but0(void);

/** Semaforo a ser usado pela task led */
SemaphoreHandle_t xSemaphore;
SemaphoreHandle_t xSemaphoreLed1;
SemaphoreHandle_t xSemaphoreLed2;
SemaphoreHandle_t xSemaphoreLed3;

#if !(SAMV71 || SAME70)
/**
 * \brief Handler for System Tick interrupt.
 */
void SysTick_Handler(void)
{
	xPortSysTickHandler();
}
#endif

/**
 * \brief Called if stack overflow during execution
 */
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName)
{
	printf("stack overflow %x %s\r\n", pxTask, (portCHAR *)pcTaskName);
	/* If the parameters have been corrupted then inspect pxCurrentTCB to
	 * identify which task has overflowed its stack.
	 */
	for (;;) {
	}
}

/**
 * \brief This function is called by FreeRTOS idle task
 */
extern void vApplicationIdleHook(void)
{
	pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
}

/**
 * \brief This function is called by FreeRTOS each tick
 */
extern void vApplicationTickHook(void)
{
}

extern void vApplicationMallocFailedHook(void)
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

	/* Force an assert. */
	configASSERT( ( volatile void * ) NULL );
}

/**
 * \brief This task, when activated, send every ten seconds on debug UART
 * the whole report of free heap and total tasks status
 */
static void task_monitor(void *pvParameters)
{
	static portCHAR szList[256];
	UNUSED(pvParameters);
	
	for (;;) {
		printf("--- Number of Tasks: %u ", (unsigned int)uxTaskGetNumberOfTasks());
		vTaskList((signed portCHAR *)szList);
		printf(szList);
		vTaskDelay(1000);
	}
}void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

/**
 * \brief This task, when activated, make LED blink at a fixed rate
 */

static void task_led0(void *pvParameters)
{
	/* Attempt to create a semaphore. */
	xSemaphore = xSemaphoreCreateBinary();

	if (xSemaphore == NULL)
	printf("FALHA - N�o foi poss�vel criar o sem�foro do LED0 \n");

	for (;;) {
		if( xSemaphoreTake(xSemaphore, ( TickType_t ) 500) == pdTRUE ){
			LED_Toggle(LED0);
		}
	}
}static void task_led1(void *pvParameters)
{
	/* Block for 500ms. -- 500 / 2 */
	const TickType_t xDelay = 500 / portTICK_PERIOD_MS;
	
	volatile bool isToggling = true;

	/* Attempt to create a semaphore. */
	xSemaphoreLed1 = xSemaphoreCreateBinary();

	if (xSemaphoreLed1 == NULL)
	printf("FALHA - N�o foi poss�vel criar o sem�foro do LED1 \n");

	for (;;) {
		if (isToggling) {
			pin_toggle(LED1_PIO, LED1_PIN_MASK);
			vTaskDelay(xDelay);
		}
		
		if (xSemaphoreTake(xSemaphoreLed1, ( TickType_t ) 0) == pdTRUE ){
			if (isToggling) {
				isToggling = false;
				pio_set(LED1_PIO, LED1_PIN_MASK);
			}
			else {
				isToggling = true;
			}
		}
	}
}static void task_led2(void *pvParameters)
{
	/* Block for 200ms. -- 200 / 2 */
	const TickType_t xDelay = 200 / portTICK_PERIOD_MS;
	printf(portTICK_PERIOD_MS);
	
	volatile bool isToggling = true;

	/* Attempt to create a semaphore. */
	xSemaphoreLed2 = xSemaphoreCreateBinary();

	if (xSemaphoreLed2 == NULL)
	printf("FALHA - N�o foi poss�vel criar o sem�foro do LED2 \n");

	for (;;) {
		if (isToggling) {
			pin_toggle(LED2_PIO, LED2_PIN_MASK);
			vTaskDelay(xDelay);
		}
		
		if (xSemaphoreTake(xSemaphoreLed2, ( TickType_t ) 0) == pdTRUE ) {
			if (isToggling) {
				isToggling = false;
				pio_set(LED2_PIO, LED2_PIN_MASK);
			}
			else {
				isToggling = true;
			}
		}
	}
}static void task_led3(void *pvParameters)
{
	/* Block for 50ms. -- 50 / 2*/
	const TickType_t xDelay = 50 / portTICK_PERIOD_MS;
	printf(portTICK_PERIOD_MS);
	
	volatile bool isToggling = true;

	/* Attempt to create a semaphore. */
	xSemaphoreLed3 = xSemaphoreCreateBinary();

	if (xSemaphoreLed3 == NULL)
	printf("FALHA - N�o foi poss�vel criar o sem�foro do LED3 \n");

	for (;;) {
		if (isToggling) {
			pin_toggle(LED3_PIO, LED3_PIN_MASK);
			vTaskDelay(xDelay);
		}
		
		if (xSemaphoreTake(xSemaphoreLed3, ( TickType_t ) 0) == pdTRUE ){
			if (isToggling) {
				isToggling = false;
				pio_set(LED3_PIO, LED3_PIN_MASK);
			}
			else {
				isToggling = true;
			}
		}
	}
}void but_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("FUNCTION - but_callback \n");
	xSemaphoreGiveFromISR(xSemaphore, &xHigherPriorityTaskWoken);
	printf("FUNCTION - but_callback - xSemaphoreGiveFromISR\n");
}

void but1_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("FUNCTION - but1_callback \n");
	xSemaphoreGiveFromISR(xSemaphoreLed1, &xHigherPriorityTaskWoken);
	printf("FUNCTION - but1_callback - xSemaphoreGiveFromISR\n");
}

void but2_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("FUNCTION - but2_callback \n");
	xSemaphoreGiveFromISR(xSemaphoreLed2, &xHigherPriorityTaskWoken);
	printf("FUNCTION - but3_callback - xSemaphoreGiveFromISR\n");
}

void but3_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("FUNCTION - but3_callback \n");
	xSemaphoreGiveFromISR(xSemaphoreLed3, &xHigherPriorityTaskWoken);
	printf("FUNCTION - but3_callback - xSemaphoreGiveFromISR\n");
}

void init_but0(void){
	/* configura bot�o como entrada */
	NVIC_EnableIRQ(BUT0_PIO_ID);
	NVIC_SetPriority(BUT0_PIO_ID, 8);
	pio_configure(BUT0_PIO, PIO_INPUT, BUT0_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(BUT0_PIO, BUT0_PIN_MASK, 60);
	pio_enable_interrupt(BUT0_PIO, BUT0_PIN_MASK);
	pio_handler_set(BUT0_PIO, BUT0_PIO_ID, BUT0_PIN_MASK, PIO_IT_FALL_EDGE , but_callback);
}

/**
* @Brief Inicializa o pino do BUT
*/
void BUT_init(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void)){
	/* configura botao como input */
	pmc_enable_periph_clk(id);
	pio_set_input(pio, mask, PIO_PULLUP | PIO_DEBOUNCE);

	/* habilita a interrup��o do bot�o, fun��o dada por handler */
	pio_enable_interrupt(pio, mask);
	pio_handler_set(pio, id, mask, PIO_IT_FALL_EDGE, handler);

	NVIC_EnableIRQ(id);
	NVIC_SetPriority(id, 2);
};


/**
* @Brief Inicializa o pino do LED
*/
void LED_init(){
	pio_set_output(LED1_PIO, LED1_PIN_MASK, 1, 0, 0);
	pio_set_output(LED2_PIO, LED2_PIN_MASK, 1, 0, 0);	
	pio_set_output(LED3_PIO, LED3_PIN_MASK, 1, 0, 0);
};


/**
 * \brief Configure the console UART.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
#if (defined CONF_UART_CHAR_LENGTH)
		.charlength = CONF_UART_CHAR_LENGTH,
#endif
		.paritytype = CONF_UART_PARITY,
#if (defined CONF_UART_STOP_BITS)
		.stopbits = CONF_UART_STOP_BITS,
#endif
	};

	/* Configure console UART. */
	stdio_serial_init(CONF_UART, &uart_serial_options);

	/* Specify that stdout should not be buffered. */
#if defined(__GNUC__)
	setbuf(stdout, NULL);
#else
	/* Already the case in IAR's Normal DLIB default configuration: printf()
	 * emits one character at a time.
	 */
#endif
}

/**
 *  \brief FreeRTOS Real Time Kernel example entry point.
 *
 *  \return Unused (ANSI-C compatibility).
 */
int main(void)
{
	/* Initialize the SAM system */
	sysclk_init();
	board_init();

	/* Initialize the console uart */
	configure_console();

	/* Output demo information. */
	printf("-- Freertos Example --\n\r");
	printf("-- %s\n\r", BOARD_NAME);
	printf("-- Compiled: %s %s --\n\r", __DATE__, __TIME__);
	
	/* Inicializa o bot�o da placa para a interrup��o do LED0*/
	init_but0();
	
	/* Inicializa os bot�es */
	BUT_init(BUT1_PIO, BUT1_PIO_ID, BUT1_PIN, BUT1_PIN_MASK, but1_callback);
	BUT_init(BUT2_PIO, BUT2_PIO_ID, BUT2_PIN, BUT2_PIN_MASK, but2_callback);
	BUT_init(BUT3_PIO, BUT3_PIO_ID, BUT3_PIN, BUT3_PIN_MASK, but3_callback);
	
	LED_init();

	/* Create task to monitor processor activity */
	if (xTaskCreate(task_monitor, "Monitor", TASK_MONITOR_STACK_SIZE, NULL,
			TASK_MONITOR_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create Monitor task\r\n");
	}

	/* Create task to make led blink */
	if (xTaskCreate(task_led0, "Led", TASK_LED_STACK_SIZE, NULL,
			TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	
	if (xTaskCreate(task_led1, "Led1", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	
	if (xTaskCreate(task_led2, "Led2", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	
	if (xTaskCreate(task_led3, "Led3", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}

	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory to create the idle task. */
	return 0;
}
